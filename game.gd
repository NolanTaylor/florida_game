extends Node2D

const gator = preload("res://gator.tscn")

func _ready():
	pass

func _on_meth_button_pressed():
	var new_gator = gator.instance()
	new_gator.set_sprite("meth")
	$gators.add_child(new_gator)

func _on_crack_button_pressed():
	var new_gator = gator.instance()
	new_gator.set_sprite("crack")
	$gators.add_child(new_gator)

func _on_opium_button_pressed():
	var new_gator = gator.instance()
	new_gator.set_sprite("opium")
	$gators.add_child(new_gator)

func _on_dmt_button_pressed():
	var new_gator = gator.instance()
	new_gator.set_sprite("dmt")
	$gators.add_child(new_gator)
