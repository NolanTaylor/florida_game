extends Node2D

const meth_texture = preload("res://Assets/meth_gator.png")
const crack_texture = preload("res://Assets/crack_gator.png")
const opium_texture = preload("res://Assets/opium_gator.png")
const dmt_texture = preload("res://Assets/dmt_gator.png")

var drag : bool = false
var anchor : Vector2 = Vector2(0, 0)

func _ready():
	pass
	
func _process(delta):
	if drag:
		position = get_global_mouse_position() - anchor
	
func set_sprite(sprite : String):
	match sprite:
		"meth":
			$sprite.set_texture(meth_texture)
		"crack":
			$sprite.set_texture(crack_texture)
		"opium":
			$sprite.set_texture(opium_texture)
		"dmt":
			$sprite.set_texture(dmt_texture)

func _on_lifespan_text_entered(new_text):
	if new_text == "-1":
		get_parent().remove_child(self)

func _on_drag_button_down():
	anchor = get_global_mouse_position() - position
	drag = true

func _on_drag_button_up():
	drag = false;
